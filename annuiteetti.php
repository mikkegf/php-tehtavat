<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lainalaskuri</title>
</head>
<body>
   <h3>Lainalaskuri</h3> 
    <?php
        $summa = filter_input(INPUT_POST,'summa',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $aika = filter_input(INPUT_POST,'aika',FILTER_SANITIZE_NUMBER_INT);
        $korko = filter_input(INPUT_POST,'korko',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);

        $kuukausikk = $korko / 100;
        $kuukaudet = $aika * 12;
        $jaettava = $kuukausikk / 12 * pow(1 + $kuukausikk / 12, $kuukaudet);
        $jakaja = pow(1 + $kuukausikk / 12, $kuukaudet) - 1;
        $vastaus = $jaettava / $jakaja * $summa;
        printf("<p>Kuukausimaksu on %.2f</p>",$vastaus);
    ?>
    <a href="index.php">Laske uudestaan</a>
</body>
</html>