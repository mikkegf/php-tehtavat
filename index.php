<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lainalaskuri</title>
</head>
<body>
    <form action="annuiteetti.php" method="post">
        <div>
            <label>Summa</label>
            <input type="number" name="summa" step="0.01" required>

        </div>
        <div>
            <label>Aika</label>
            <select name="aika">
                <?php
                for ($i = 1;$i <= 25;$i++) {
                    printf ("<option>$i</option>",);
                }
                ?>
            </select>
        </div>
        <div>
            <label for="">Korko</label>
            <select name="korko">
                <?php
                for ($i = 0;$i <= 5;$i= $i + 0.25) {
                    printf ("<option>%.2f</option>", $i);
                }
                ?>
            </select>
        </div>
        <button>Laske</button>
    </form>
</body>
</html>